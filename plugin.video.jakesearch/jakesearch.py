import xbmc, xbmcgui, xbmcaddon, urllib, urllib2, json, xbmcplugin
 
#get actioncodes from https://github.com/xbmc/xbmc/blob/master/xbmc/guilib/Key.h
ACTION_PREVIOUS_MENU = 10
SEARCHURL = "http://beastie/cgi-bin/searchjson.cgi?q="
#SEARCHURL = "http://camera.jakebriggs.com:81/cgi-bin/searchjson.cgi?q="
#SEARCHURL = "http://osama2/cgi-bin/searchjson.cgi?q="

addon_id = int(sys.argv[1])
xbmcplugin.setContent(addon_id, 'movies')

def onAction(self, action):
  if action == ACTION_PREVIOUS_MENU:
    self.close()

keyboard = xbmc.Keyboard('')
keyboard.doModal()
	
if (keyboard.isConfirmed()):
    req = urllib2.Request(SEARCHURL + urllib.pathname2url(keyboard.getText()))
    line3 = urllib2.urlopen(req).read().decode("utf-8")
    data = json.loads(line3)
        
    for i in data:
        vidname = i.split("/")[-1];
        fullpathname = i[0:-4]
        listitem = xbmcgui.ListItem(vidname, fullpathname, '', fullpathname+'.jpg', i)
        listitem.setInfo('Video', { })
        xbmcplugin.addDirectoryItem(handle = addon_id, url = i, listitem = listitem)
    
    xbmcplugin.endOfDirectory(addon_id)

else:
    self.strActionInfo.setLabel('user canceled')
 







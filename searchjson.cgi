#!/bin/bash

VIDPATH="/data/Video/"
Q=$(echo "$QUERY_STRING" | sed -n 's/^.*q=\([^&]*\).*$/\1/p' | sed "s/%20/ /g")

echo -e "Content-type: text/plain\n"

find "${VIDPATH}" -type f  \( -iname "*$Q*" ! -iname "*.jpg" ! -iname "*.xml" ! -iname "*.srt" ! -iname "*.fxd" ! -iname "*.txt" ! -iname "*.png" ! -iname "*.fxd" ! -iname "*.rar" ! -iname "*.gif" ! -iname "*.sub" ! -iname "*.idx" \) |\
	sed -r '
		s/.*/"\0"/
		1!{s/^/,/}
		1{s,^,[,}
		${s,$,],}
	'

exit 0

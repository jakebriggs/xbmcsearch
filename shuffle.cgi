#!/bin/bash

MUSICPATH="/data/music/"
Q=$(echo "$QUERY_STRING" | sed -n 's/^.*q=\([^&]*\).*$/\1/p' | sed "s/%20/ /g")

echo -e "Content-type: text/plain\n"

#/usr/bin/find $MUSICPATH -iname "*mp3" | /usr/bin/sort -R | /usr/bin/head -n 100 |\ 
#/usr/bin/find $MUSICPATH -iname "*mp3" | /usr/bin/sort -R | /usr/bin/head -n $Q |\
/usr/bin/find $MUSICPATH -iname "*mp3" | /usr/bin/sort -R |\
	sed -r '
		s/.*/"\0"/
		1!{s/^/,/}
		1{s,^,[,}
		${s,$,],}
	'

exit 0
